var img;
var width = document.getElementById('fun-image').offsetWidth;
var imageWidth = width/2;
var imageHeight = imageWidth;
var vScale = 20;
var scaledHeight = Math.floor(imageHeight / vScale);
var scaledWidth = Math.floor(imageWidth / vScale);
var scaledImageLength = scaledHeight * scaledWidth * 4;
var scaledImage = new Array(scaledImageLength).fill(0);

function preload() {
  img = loadImage('images/82.jpg');
  pixelDensity(1);
}

function setup() {
  var funPic = createCanvas(imageWidth * 2, imageHeight);
  funPic.parent('fun-image');
}

function draw() {
  image(img, 0, 0, imageWidth, imageHeight);
  loadPixels();
  for (var y = 0; y < imageHeight; y++ ) {
    for (var x = 0; x < imageWidth; x++ ) {
      var index = ( x + y * imageWidth ) * 4;

      var r = pixels[index + 0];
      var b = pixels[index + 1];
      var g = pixels[index + 2];
      var brightness = (r + b + g) / 3;
      pixels[index + 0] = brightness;
      pixels[index + 1] = brightness;
      pixels[index + 2] = brightness;
      pixels[index + 3] = 255;
    }
  }
  updatePixels();
}
