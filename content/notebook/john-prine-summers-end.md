---
title: "John Prine - Summer's End"
date: 2018-06-26T17:42:57-04:00
draft: false
tags: ["music"]
textSize: "6"
style: "grey"
---

<div class="intrinsic-container intrinsic-container-16x9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9XLaIFTmJF8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
