---
title: "The Langlois Bridge"
date: 2018-07-15T13:58:26-04:00
draft: false
source: "Van Gogh Museum"
sourceLink: "https://www.vangoghmuseum.nl/en/collection/s0027V1962"
tags: ["art", "van gogh"]
textSize: "6"
image: "langlois-bridge.png"
alt: "The Langlois Bridge by Vincent Van Gogh"
style: "grey"
---
