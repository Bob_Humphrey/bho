---
title: "We Still Have to Care"
date: 2018-06-17T11:55:02-04:00
draft: false
source: "It’s All Too Much, and We Still Have to Care - Dahlia Lithwick,
Slate"
sourceLink: "https://slate.com/news-and-politics/2018/06/family-separation-is-horrifying-but-we-cant-go-numb-and-turn-away.html"
tags: ["trump", "resistance"]
textSize: "6"
style: "grey"
---
<blockquote>
Numbness is something thrust upon us, a physical or emotional reaction to external shocks, a natural bodily response. It is also maybe a buffer we put up against the devastation of being part of a group that is constantly told it is worthless and undeserving of meaningful attention.
<br><br>
That we are finding ourselves unable to process or act or organize because the large-scale daily horrors are escalating and the news is overpowering is perfectly understandable. But we need to understand that and acknowledge it and then refuse it any purchase. Because to be overwhelmed and to do nothing are a choice.
</blockquote>
