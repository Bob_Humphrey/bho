---
title: "White American Support for Authoritarianism"
date: 2018-05-31T12:05:24-04:00
draft: false
source: "The Trump effect: New study connects white American intolerance and
support for authoritarianism - Noah Berlatsky, NBC News"
sourceLink: "https://www.nbcnews.com/think/opinion/trump-effect-new-study-connects-white-american-intolerance-support-authoritarianism-ncna877886"
tags: ["news", "authoritarianism", "trump"]
style: "one"
textSize: "6"
style: "grey"
---
<blockquote>
Political scientists Steven V. Miller of Clemson and Nicholas T. Davis of
Texas A&M have released a working paper titled "White Outgroup Intolerance and
Declining Support for American Democracy." Their study finds a correlation
between white American's intolerance, and support for authoritarian rule. In
other words, when intolerant white people fear democracy may benefit
marginalized people, they abandon their commitment to democracy.
<br><br>
Based on surveys from the United States, the authors found that white people who
did not want to have immigrants or people of different races living next door to
them were more likely to be supportive of authoritarianism. For instance,
people who said they did not want to live next door to immigrants or to people
of another race were more supportive of the idea of military rule, or of a
strongman-type leader who could ignore legislatures and election results.
</blockquote>
