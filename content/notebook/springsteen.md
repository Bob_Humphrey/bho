---
title: "Bruce Springsteen - My  Hometown"
date: 2018-06-12T11:19:19-04:00
draft: false
tags: ["music"]
textSize: "6"
style: "link"
---

<div class="intrinsic-container intrinsic-container-16x9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/77gKSp8WoRg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
