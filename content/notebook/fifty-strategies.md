---
title: "Fifty Strategies for Fighting Trump"
date: 2018-07-15T14:36:30-04:00
draft: false
source: "ZawnVillines, Daily Kos"
sourceLink: "https://www.dailykos.com/stories/2018/7/5/1778150/-50-Strategies-for-Fighting-Trump-Ideas-for-All-Ability-Levels-Incomes-and-Time-Constraints"
tags: ["trump", "resistance"]
textSize: "6"
style: "primary"
---
<blockquote>
Some analysts have suggested that, if just 3.5% of the population actively resists, it’s possible to topple a dictator. So fighting back against an authoritarian regime in a democratic society should be even easier.
</blockquote>
