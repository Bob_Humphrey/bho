---
title: "How to Fight Trump and Stay Psychologically Healthy"
date: 2018-06-28T19:07:03-04:00
draft: false
source: "ZawnVillines, Daily Kos"
sourceLink: "https://www.dailykos.com/stories/2018/6/28/1776249/-Authoritarianism-Thrives-on-Demoralization-How-to-Fight-Trump-and-Stay-Psychologically-Healthy"
tags: ["trump", "authoritarianism", "resistance"]
textSize: "6"
style: "link"
---
<blockquote>
Authoritarian regimes thrive on a demoralized populace. They survive by slowly wearing us down. By convincing us there’s nothing we can do. And now so many of my friends are doing that work on their behalf. It’s time to stop. They’re throwing up their hands, saying everyone is doomed, and devoting their precious energy to convincing everyone how bad the world will soon be.
<br><br>

I don’t know what the future holds or if the doomsday scenarios will come to fruition. I do know that spending our time paralyzed in panic only emboldens Trump and his ilk. Feeling depressed feels awful. It’s also what the Trump regime wants because it reduces a person’s effectiveness.
<br><br>

So here is my modest recipe for fighting back against the Trump regime, protecting yourself from panic and depression, and maintaining a little perspective.
</blockquote>
