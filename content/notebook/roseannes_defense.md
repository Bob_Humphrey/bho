---
title: "In Roseanne's Defense"
date: 2018-06-01T11:21:37-04:00
draft: false
tags: ["trump", "racism"]
style: "three"
textSize: "6"
style: "grey"
---

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">In Roseanne’s defense, it’s hard to know the difference between racism that gets you fired versus racism that gets you elected President of the United States.</p>&mdash; Brian Christopher (@bcsproul) <a href="https://twitter.com/bcsproul/status/1001550525651931136?ref_src=twsrc%5Etfw">May 29, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
