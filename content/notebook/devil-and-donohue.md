---
title: "The Devil and Tom Donohue"
date: 2018-06-21T18:33:07-04:00
draft: false
source: "Paul Krugman, The New York Times"
sourceLink: "https://www.nytimes.com/2018/06/20/opinion/the-devil-and-tom-donohue.html"
tags: ["trump", "racism"]
textSize: "6"
style: "grey"
---
<blockquote>
News item #1: The Trump administration is taking thousands of children away from their parents, and putting them in cages.
<br><br>

News item #2: House Republicans have released a budget plan that would follow up last year’s big tax cuts for corporations and the wealthy with huge funding cuts for Medicare and Medicaid.
<br><br>

If you think these items are unrelated, you’ve missed the whole story of modern American politics. Conservatism – the actually existing conservative movement, as opposed to the philosophical stance whose constituency is maybe five pundits on major op-ed pages — is all about a coalition between racists and plutocrats. It’s about people who want to do (2) empowering people who want to do (1), and vice versa.
<br><br>

The conservative economic agenda has never been popular, and it is objectively against the interests of working class voters, whatever their race. In fact, whites without a college degree are the biggest beneficiaries of the social safety net. Nonetheless, these voters supported the GOP because it spoke to their racial animosity.
<br><br>

Now, to be fair, for a long time the GOP establishment managed to pull off a kind of bait-and-switch with this strategy, using race to motivate voters who would otherwise oppose its agenda, then putting the racism back in the closet once in office.
<br><br>

Ultimately, however, this bait-and-switch was bound to be unsustainable. Sooner or later the people who voted for white dominance at their own economic expense were going to find a champion who would deliver on their side of the bargain.
<br><br>

And now they have. Trump doesn’t say “nigger, nigger, nigger” – or at least I haven’t seen that. But he does talk about “animals” trying to “infest” America, which is arguably even more dehumanizing. And the cruelty of his border policy actually surpasses the days of Jim Crow; it harks back to the slave trade.
</blockquote>
