---
title: "Why are the lights on?"
date: 2018-06-09T11:18:06-04:00
draft: false
source: "Has Consciousness Lost Its Mind? - The Chronicle of Higher Education"
sourceLink: "https://www.chronicle.com/article/is-this-the-world-s-most/243599"
tags: ["consciousness"]
style: "one"
textSize: "5"
style: "grey"
---
<blockquote>
The scientists who embrace the mystical stuff can’t explain the hard problem of consciousness either. No one can explain it. Why does it feel like something to be you? What is it that makes us more than just information processors with feet? Why are the lights on and who, precisely, is at home?
<br><br>
Nobody knows.
</blockquote>
