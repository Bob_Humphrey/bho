---
title: "Jesus stands where he always stands"
date: 2018-06-15T14:45:49-04:00
draft: false
source: "Blindly following the law is not “biblical” - James Martin SJ, America Magazine"
sourceLink: "https://www.americamagazine.org/politics-society/2018/06/15/father-james-martin-blindly-following-law-not-biblical"
tags: ["trump", "christianity"]
textSize: "6"
style: "primary"
---
<blockquote>
Over and over in the Gospels Jesus takes the side of those who are beaten or persecuted in any way, even when it brings him into conflict with not only the social mores of his day but with religious and political laws.
<br><br>
And in case it is not clear: It is not biblical to treat migrants and refugees like animals. It is not biblical to take children away from their parents. It is not biblical to ignore the needs of the stranger. It is not biblical to enforce unjust laws. The Bible should not be used to justify sin.
<br><br>
It should also be clear where Jesus stands on these questions. Jesus stands where he always stands and where we should stand: with the poor and marginalized.
</blockquote>
