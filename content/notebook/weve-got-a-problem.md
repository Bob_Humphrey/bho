---
title: "We've Got a Problem"
date: 2018-06-09T12:30:17-04:00
draft: false
source: "Josh Marshall, Talking Points Memo"
sourceLink: "https://talkingpointsmemo.com/edblog/weve-got-a-problem-a-big-problem"
tags: ["trump"]
textSize: "6"
style: "grey"
---
<blockquote>
The last twenty four hours of attacks on our closest allies capped by President Trump’s seemingly out of the blue demand to bring Russia back into the G-7 ... simply brings the matter into a newly sharp relief. If candidate Trump and President Putin had made a corrupt bargain which obligated President Trump to destabilize all U.S. security and trade alliances (especially NATO, which has been Russia’s primary strategic goal for 70 years) and advance the strategic interests of Russia, there’s really nothing more remotely realistic he could have done to accomplish that than what he has in fact done.
<br><br>
Take a moment to let that sink in. I need to take a moment to let it sink in. It’s shocking to me.
<br><br>
... We have a President who clearly got a great deal of assistance from Russia in getting elected. We can argue about how important it was to his victory. But the reality of the help is not in any real dispute. His campaign at a minimum had numerous highly suspicious contacts with people either in the Russian government or acting on behalf of the Russian government while that was happening. That is a very generous interpretation. He’s doing all the stuff he’d have been asked to do if such a corrupt bargain had been made. At a certain point – and I’d say we’re clearly at or past that point – it really doesn’t matter whether we can prove such a bargain was made. I’m not even sure it matters whether it was explicit or even happened. The bank robber helped the teller get the job and now the teller just won’t seem to lock the safe or even turn on the alarm. We can debate forever whether the teller is just absent-minded or has some odd philosophical aversion toward locks. The debate may be unresolvable. It truly doesn’t matter.
</blockquote>
