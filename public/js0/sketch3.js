var img;
var q;
var width = document.getElementById('fun-image').offsetWidth;
var imageWidth = width;
var imageHeight = imageWidth;
var vScale = 150;
var vScaleMax = 150;
var container = 'fun-image';
var prevMills = 0;
var diffMills = 0;
var densityDirection = 'lower';
var slowTransormSpeed = 500;
var fastTransformSpeed = 250;
var fastTransformThreshold = 20;

function preload() {
  img = loadImage('images/82.jpg');
  pixelDensity(1);
}

function setup() {
  manipulateImage( vScale, img, imageWidth, imageHeight, container );
}

function draw() {
  let mills = millis();
  diffMills = mills - prevMills;
  if ((( vScale >= fastTransformThreshold ) && ( diffMills > fastTransformSpeed )) ||
    (( vScale < fastTransformThreshold ) && ( diffMills > slowTransormSpeed ))) {
    prevMills = mills;
    if (densityDirection === 'lower') {
      vScale--;
      if (vScale < 1) {
        vScale = 1;
        densityDirection = 'higher';
      }
      manipulateImage( vScale, img, imageWidth, imageHeight, container );
    }
    else if (densityDirection === 'higher'){
      vScale++;
      if (vScale > vScaleMax ) {
        vScale = vScaleMax;
        densityDirection = 'lower';
      }
      manipulateImage( vScale, img, imageWidth, imageHeight, container );
    }
  }

}

function manipulateImage( vScale, img, imageWidth, imageHeight, container) {
  console.log('run');
  let x, y, r, b, h, index;
  let scaledX = 0.0;
  let scaledY = 0.0;
  let scaledIndex = 0;
  let scaledR = 0.0;
  let scaledB = 0.0;
  let scaledG = 0.0;
  let scaledResolution = vScale * vScale;
  let scaledHeight = Math.floor(imageHeight / vScale);
  let scaledWidth = Math.floor(imageWidth / vScale);
  let scaledImageLength = scaledHeight * scaledWidth * 4;
  let scaledPixels = new Array(scaledImageLength).fill(0);
  let manipulatedImage = createCanvas(imageWidth, imageHeight);

  manipulatedImage.parent(container);
  image(img, 0, 0, imageWidth, imageHeight);
  loadPixels();

  // For each pixel in the image, read the associated values from the
  // pixels[] array. Store that information into the scaledPixels array.

  for (y = 0; y < imageHeight; y++ ) {
    for (x = 0; x < imageWidth; x++ ) {
      index = ( x + y * imageWidth ) * 4;
      scaledX = Math.floor(x / vScale);
      scaledY = Math.floor(y / vScale);
      if (scaledX < scaledWidth && scaledY < scaledHeight) {
        scaledIndex = ( scaledX + scaledY * scaledWidth ) * 4;
        r = pixels[index + 0];
        b = pixels[index + 1];
        g = pixels[index + 2];
        scaledR = scaledPixels[scaledIndex + 0];
        scaledB = scaledPixels[scaledIndex + 1];
        scaledG = scaledPixels[scaledIndex + 2];
        scaledR += r;
        scaledB += b;
        scaledG += g;
        scaledPixels[scaledIndex + 0] = scaledR;
        scaledPixels[scaledIndex + 1] = scaledB;
        scaledPixels[scaledIndex + 2] = scaledG;
      }
    }
  }

  // For each pixel in the image, read the scaled data in the scaledPixels
  // array and update the pixels array.

  for (y = 0; y < imageHeight; y++ ) {
    for (x = 0; x < imageWidth; x++ ) {
      index = ( x + y * imageWidth ) * 4;
      scaledX = Math.floor(x / vScale);
      scaledY = Math.floor(y / vScale);
      if (scaledX < scaledWidth && scaledY < scaledHeight) {
        scaledIndex = ( scaledX + scaledY * scaledWidth ) * 4;
        scaledR = scaledPixels[scaledIndex + 0] / scaledResolution;
        scaledB = scaledPixels[scaledIndex + 1] / scaledResolution;
        scaledG = scaledPixels[scaledIndex + 2] / scaledResolution;
        pixels[index + 0] = Math.floor(scaledR);
        pixels[index + 1] = Math.floor(scaledB);
        pixels[index + 2] = Math.floor(scaledG);
      }
      else {
        pixels[index + 0] = 0;
        pixels[index + 1] = 0;
        pixels[index + 2] = 0;
        pixels[index + 3] = 0;
      }
    }
  }

  updatePixels();
}
